Simple Mapping Service, that maps Plot Numbers to GPS's coordinates.

The service entry point is:

https://plot-number-mapper.herokuapp.com/plotNumbers/

Calling the entry point will list all Plot Numbers registered in the system, in other words, numbers that can be mapped.

To get GPS coordinates for, say Plot Number 37493, construct the following URI:


https://plot-number-mapper.herokuapp.com/plotNumbers/37493   
 