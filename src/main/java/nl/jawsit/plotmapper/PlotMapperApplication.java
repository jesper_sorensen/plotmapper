package nl.jawsit.plotmapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class PlotMapperApplication {


   public static void main(String[] args) {
      SpringApplication.run(PlotMapperApplication.class, args);
   }

}
