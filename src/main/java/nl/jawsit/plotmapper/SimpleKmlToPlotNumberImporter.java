package nl.jawsit.plotmapper;

import de.micromata.opengis.kml.v_2_2_0.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class SimpleKmlToPlotNumberImporter {

   private static final String PLOT_NUMBER = "PLOTNUMBER";
   private static final SimpleData UNKNOWN_PLOT_NUMBER = new SimpleData(PLOT_NUMBER).withValue("Unknown");

   private final PlotNumberRepo repo;

   private static Coordinate findCenterPoint(List<Coordinate> coordinates) {

      AtomicReference<Double> sumLongitude = new AtomicReference<>((double) 0);
      AtomicReference<Double> sumLatitude = new AtomicReference<>((double) 0);
      AtomicReference<Double> sumAltitude = new AtomicReference<>((double) 0);

      coordinates.forEach(coordinate -> {
         sumLongitude.updateAndGet(v -> ((v + coordinate.getLongitude())));
         sumLatitude.updateAndGet(v -> ((v + coordinate.getLatitude())));
         sumAltitude.updateAndGet(v -> ((v + coordinate.getAltitude())));
      });

      int size = coordinates.size();

      return new Coordinate(sumLongitude.get() / size, sumLatitude.get() / size, sumAltitude.get() / size);

   }

   public int execute() {

      InputStream in = this.getClass().getClassLoader().getResourceAsStream("GC_Block8_Parcels.kml");

      final Kml kml = Kml.unmarshal(in);

      Document document = (Document) kml.getFeature();

      Folder folder = (Folder) document.getFeature().get(0);

      List<Feature> folderFeatures = folder.getFeature();

      List<PlotNumber> plotNumbers =
          folderFeatures.stream()
              .map(folderFeature -> new PlotNumber(getPlotNumberString(folderFeature), getPlotCoordinates(folderFeature)))
              .filter(plotNumber -> !plotNumber.getId().equals(UNKNOWN_PLOT_NUMBER.getValue()))
              .collect(Collectors.toList());
      repo.saveAll(plotNumbers);

      return plotNumbers.size();

   }

   private String getPlotNumberString(Feature folderFeature) {
      List<SimpleData> simpleData = folderFeature.getExtendedData().getSchemaData().get(0).getSimpleData();

      Optional<SimpleData> optionalSimpleData = simpleData.stream().filter(simpleData1 -> simpleData1.getName().equalsIgnoreCase(PLOT_NUMBER)).findFirst();

      return optionalSimpleData.orElse(UNKNOWN_PLOT_NUMBER).getValue();
   }

   private Coordinate getPlotCoordinates(Feature folderFeature) {
      List<Coordinate> coordinates = ((Polygon) (((Placemark) folderFeature).getGeometry())).getOuterBoundaryIs().getLinearRing().getCoordinates();
      return findCenterPoint(coordinates);
   }
}
