package nl.jawsit.plotmapper;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PlotMapperConfig {

   @Bean
   CommandLineRunner init(SimpleKmlToPlotNumberImporter importer) {
      return (evt) -> {
         int execute = importer.execute();
         System.out.println("Imported rows of data: " + execute);


      };
   }

}
