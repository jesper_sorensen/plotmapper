package nl.jawsit.plotmapper;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RestResource
public interface PlotNumberRepo extends JpaRepository<PlotNumber, String> {
}
