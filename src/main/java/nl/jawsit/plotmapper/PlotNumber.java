package nl.jawsit.plotmapper;


import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class PlotNumber {


   @Id
   private String id;
   private double longitude;
   private double latitude;
   private double altitude;

   public PlotNumber(String plotNumber, double longitude, double latitude, double altitude) {
      this.id = plotNumber;
      this.longitude = longitude;
      this.latitude = latitude;
      this.altitude = altitude;
   }

   public PlotNumber(String plotNumber, double longitude, double latitude) {
      this(plotNumber, longitude, latitude, 0);
   }

   public PlotNumber(String plotNumber, Coordinate coordinate) {
      this(plotNumber, coordinate.getLongitude(), coordinate.getLatitude(), coordinate.getAltitude());
   }


}
