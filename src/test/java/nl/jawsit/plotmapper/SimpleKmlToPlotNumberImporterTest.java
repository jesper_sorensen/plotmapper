package nl.jawsit.plotmapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.FileNotFoundException;

@RunWith(MockitoJUnitRunner.class)
public class SimpleKmlToPlotNumberImporterTest {

   @Mock
   PlotNumberRepo repo;

   @Test
   public void test() throws FileNotFoundException {

      SimpleKmlToPlotNumberImporter uut = new SimpleKmlToPlotNumberImporter(repo);

      int execute = uut.execute();
      System.out.println("Imported rows of data: " + execute);


   }

}